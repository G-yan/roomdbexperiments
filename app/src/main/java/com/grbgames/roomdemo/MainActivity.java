package com.grbgames.roomdemo;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.grbgames.roomdemo.localdb.EHDatabase;
import com.grbgames.roomdemo.localdb.EventAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText etName, etDesc;
    private String[] stringArray;
    private int count = 0;
    private Button btnAddEvent;
    private RecyclerView recEvent;
    private EHDatabase database;
    private List<Event> events;
    private EventAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.et_evnt_name);
        etDesc = findViewById(R.id.et_evnt_desc);
        stringArray = getResources().getStringArray(R.array.images);
        btnAddEvent = findViewById(R.id.btn_add_evnt);
        recEvent = findViewById(R.id.rv_evnt);

        btnAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eventName = String.valueOf(etName.getText());
                String eventDesc = String.valueOf(etDesc.getText());
                double currTime = System.currentTimeMillis() / 1000;


                Event event = new Event(eventName, eventDesc, stringArray[count], currTime);


                database.eventDao().addEvent(event);

                if (count == stringArray.length - 1)
                    count = 0;

                events = database.eventDao().getEvents();
                eventAdapter = new EventAdapter(events);

                recEvent.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false));

                recEvent.setAdapter(eventAdapter);
                eventAdapter.notifyDataSetChanged();
                count++;
            }
        });
        database = Room.databaseBuilder(MainActivity.this, EHDatabase.class, "db-events")
                .allowMainThreadQueries()   //Allows room to do operation on main thread
                .build();

        events = database.eventDao().getEvents();

        eventAdapter = new EventAdapter(events);

        recEvent.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        recEvent.setAdapter(eventAdapter);

    }
}
