package com.grbgames.roomdemo.localdb;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grbgames.roomdemo.Event;
import com.grbgames.roomdemo.GlideApp;
import com.grbgames.roomdemo.R;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    List<Event> events;

    public EventAdapter(List<Event> events) {
        this.events = events;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvEvntName.setText(events.get(position).eventName);
        holder.tvEvntDesc.setText(events.get(position).eventDesc);
        GlideApp.with(holder.itemView.getContext())
                .load(events.get(position).eventIcon)
                .into(holder.imgEvntLogo);
    }

    @Override
    public int getItemCount() {
        return events != null ? events.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvEvntName;
        TextView tvEvntDesc;
        ImageView imgEvntLogo;
        public ViewHolder(View itemView) {
            super(itemView);

            tvEvntName = itemView.findViewById(R.id.tv_evnt_name);
            tvEvntDesc = itemView.findViewById(R.id.tv_evnt_desc);
            imgEvntLogo = itemView.findViewById(R.id.evnt_logo);
        }
    }
}
