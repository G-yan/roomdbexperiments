package com.grbgames.roomdemo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;


import org.jetbrains.annotations.NotNull;

@Entity(tableName = "events")
public class Event {
    @ColumnInfo(name = "event_name")
    public String eventName;

    @ColumnInfo(name = "event_desc")
    public String eventDesc;

    @ColumnInfo(name = "event_icon")
    public String eventIcon;
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "event_id")
    @NotNull
    public int ID;

    @ColumnInfo(name = "event_updated_on")
    public double eventUpdatedOn;

    @Ignore
    public Event(String eventName, String eventDesc, String eventIcon, double eventUpdatedOn) {
        this.eventName = eventName;
        this.eventDesc = eventDesc;
        this.eventIcon = eventIcon;
        this.eventUpdatedOn = eventUpdatedOn;

    }

    public Event() {
    }
}
