package com.grbgames.roomdemo.localdb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.grbgames.roomdemo.Event;

import java.util.List;

@Dao
public interface EventDao {

    @Insert
    void addEvent(Event event);

    @Query("select * from events")
    List<Event> getEvents();
}
