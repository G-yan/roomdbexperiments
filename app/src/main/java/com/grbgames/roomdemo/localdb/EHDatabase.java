package com.grbgames.roomdemo.localdb;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.grbgames.roomdemo.Event;


@Database(entities = {Event.class},version = 1,exportSchema = false)
public abstract class EHDatabase extends RoomDatabase {
    public abstract EventDao eventDao();
}
